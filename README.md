# Bevakning nybilsförsäljning och utsläpp från transportsektorn

Det här repot innehåller data och analys över nybilsförsäljning av personbilar med särskilt fokus på betydelsen för koldioxidutsläpp och möjligheterna att nå klimatmålen i transportsektorn.

Den ligger till grund för publiceringar på i första hand newsworthy.se, men också DI.se.

## Struktur

- `charts`: Genererade grafer.
- `data`: Indata från olika källor.
- `notebooks`: Jupyter notebooks för datanalys och grafgenerering.

## Kontakt

Jens Finnäs: jens@newsworthy.se
